interface Task {
  title: string;
  status: boolean;
}

class PageObject {
  public tasksPromise: Task[] = [{ title: 'one', status: true }, { title: 'two', status: false }];

  constructor() {}

  async getAll(): Promise<Task[]> {
    return await this.tasksPromise;
  }
}

// Возвращаемый тип данных - Promise, но при этом с помощью угловых скобок <Task[]>
// мы указали, что именно в себе хранит этот Promise. Когда мы делаем любую асинхронную
// операцию, которая возвращает Promise, мы понимаем, что в Promise хранится значение,
// которое появится через какое-то время. С помощью угловых скообок мы сообщаем это значение.
// В данном случае Promise будет содержать массив задач. <Task[]>
