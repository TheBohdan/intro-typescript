// TypeScript позволяет определять псевдонимы типов с помощью ключевого слова type.
// Затем мы сможем использовать псевдоним аналогично типу данных.

type strOrNumType = number | string;

let sum: strOrNumType = 3;
if (typeof sum === 'number') {
  console.log(sum / 6);
}
