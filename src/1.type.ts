/*

Все что приведено ниже ставится после объявления переменной:

: string
: number
: boolean
: any
: string | number
: number | null

*/

// Типы массивов

/*
: number[]
: string[]
*/

// OR

/*
: Array<number> // обозначение дженерика
*/

// С использованием интерфейса и дженерика:
let arr: Array<number>;

// : [number, number, string]

let a: number;
let b: boolean;
let c: string;
let d: any;
let e: number[] = [1, 2, 3, 4, 5];
let f: any[] = [1, true, 'a', false];
