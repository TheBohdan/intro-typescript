// здесь <T> дженерик, который будет использоваться в ф-и
// само T это сокращение от Type

function genericGetter<T>(data: T): T {
  return data;
}

// Дженерик самостоятельно определяет тип данных:
console.log(genericGetter('Test').length);

let newGenericFunction: <T>(data: T) => T = genericGetter; // <T>(data: T) => T это тип

// Самостоятельно указываем тип данных дженерика:
console.log(genericGetter<string>('Test').length);

// 2-й пример:

// Дженерик может принимать только number или string
class Multiply<T extends number | string> {
  constructor(private a: T, private b: T) {}

  public getResult(): number {
    return +this.a * +this.b;
  }
}

const mNum = new Multiply<number>(10, 5);
console.log('Number: ', mNum.getResult());

const mStr = new Multiply<string>('50', '60');
console.log('String: ', mStr.getResult());
