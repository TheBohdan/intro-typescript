// Абстрактные классы
// От абстрактных классов мы не можем делать экземпляров.
// Абстрактные классы нужны лишь для того, чтобы от них наследовались.

abstract class IUser {
  name?: string;
  year: number = 1982;

  // метод абстрактный, поэтому в классе, например,
  // Employee он может быть реализован со своей логикой
  abstract logInfo(info: string): void;

  public getYear(): number {
    return this.year;
  }
}

class Employee extends IUser {
  public logInfo(info: string): void {
    console.log(info);
  }
}

const john = new Employee();
console.log(john);

john.logInfo('Info'); // info
console.log(john.getYear()); // 1982
