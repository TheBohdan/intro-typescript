// При работе с классами интерфейсы определяют контракт (implements)
// указывающий, что классы реализующие этот интерфейс должны определить
// определенные свойства и методы.

// Например, интерфейс OnInit:

interface OnInit {
  foo?: string;
}

export class ListArticles implements OnInit {}

// Что нужно знать об интерфейсах: интерфейсы используются только
// во время компиляции TypeScript, и затем удаляются.
// В конечном JavaScript их не будет.

// Привести пустой объект к типу интерфейса Task
const task = <Task>{};

interface Task {
  title: string;
  status: boolean;
}

console.log(task);

// task.
