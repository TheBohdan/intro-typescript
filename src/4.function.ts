// Функции мы можем записывать так:
let fun: (a: string, b: number) => string;

// Первый пример создан на основе ES6 синтаксиса. Еще несколько примеров:
type User = {
  name: string;
  age: number;
  getJobs: () => string[];
  jobs: string[];
  logName: (prefix: string) => void; // название аргументов неважно, главное ТИП
};

let user: User = {
  name: 'John',
  age: 21,
  jobs: ['a', 'b'],
  getJobs(): string[] {
    return this.jobs;
  },
  logName(prefix: string): void {
    console.log(prefix + this.name);
  },
};

user.logName('hello'); //hello John
