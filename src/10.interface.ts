// Наследование интерфейсов
// Интрефейсы в отличие от type можно наследовать:

interface IMove {
  move(): void; // как видите для описания функции мы можем использовать 2 сигнатуры
  getCount: () => { count: number };
}

interface IShake {
  shake(): void;
  getCount: () => { frequency: number };
}

interface IMoveShake extends IShake, IMove {
  getCount: () => { count: number; frequency: number };
}
