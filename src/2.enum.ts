// С помощью enum мы можем создавать константы и затем использовать их как тип.
// enum

const ColorRed = 0;
const ColorGreen = 1;
const ColorBlue = 2;

enum ColorOne {
  Red,
  Green,
  Blue,
}
let backgroundColor = ColorOne.Red; // 0

// or
enum ColorTwo {
  Red = 0,
  Green = 1,
  Blue = 2,
  Purple = 3,
}

console.log(ColorTwo.Purple); // 3
