// определение массива чисел
let list1: number[] = [1, 2, 3];
// определение массива чисел с использования generic типа Array<elemType>
let list2: Array<number> = [1, 2, 3];

import { Phrase } from './14._phrase';
// определяем массив Phrase
let arrPhrase: Phrase[] = [{ value: 'Hello World', language: 'English' }];

// Assertions (утверждение типа)
// assertions бывают двух видов - <> или as

let message; // by default type any
message = 'abc';

// Метод endsWith() определяет, заканчивается ли строка символами другой строки, возвращая, соотвественно, true или false.
// https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith

// принудительно указывает тип для переменной message
let endWithC = (<string>message).endsWith('c');
let alternativeEndWithC = (message as string).endsWith('c');

// Как задать объект с произвольным кол-вом свойств?

let hashMap: {
  [key: string]: number | string;
} = {};

// key - незарезервированное, может быть любое слово

// Модификатор readonly

let account: {
  readonly age: number;
  name: string;
} = {
  age: 35,
  name: 'John',
};

// account.age = 44; // ошибка
