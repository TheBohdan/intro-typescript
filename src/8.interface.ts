// Интерфейс - описание формы объекта, в котором можно описать
// как обязательные так и необязательные поля.

// Можно описать тип так:

let drawPointOne = (point: { x: number; y: number }) => {
  //..
};

drawPointOne({
  x: 1,
  y: 2,
});

// Но, как вы видите, это решение актуально для лишь конкретной функции (drawPoint).
// Обойти эту проблему поможет использование интерфейсов.

interface Point {
  x: number;
  y: number;
}

let drawPoint = (point: Point) => {
  //..
};

drawPoint({
  x: 1,
  y: 2,
});

// еще 1 пример

interface ITodo {
  title: string;
  completed: boolean;
}

const todo: ITodo = {
  title: 'title',
  completed: true,
};
