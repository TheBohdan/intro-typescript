// Параметры по умолчанию - после типа переменной указываем его значение по умолчанию.
// greeting: string = 'Hello' 'Hello' как параметр по умолчанию
function greetMe(name: string, greeting: string = 'Hello'): string {
  return greeting + name;
}

// Добавим символ ? в конце параметра, тем самым сделаем такой параметр необязательным.
function greetMeTwo(name: string, greeting?: string): string {
  // : string означает, что возвращает строку
  return greeting ? greeting : 'Hello' + ', ' + name;
}

// Или можно использовать непосредственно в шаблоне:
// <b> {{currentUser?.nickname}} </b>
